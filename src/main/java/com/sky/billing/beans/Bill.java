package com.sky.billing.beans;

import org.codehaus.jackson.annotate.JsonProperty;

public class Bill {
    private CallCharges callCharges;
    private Package skyPackage;
    private SkyStore skyStore;
    private Statement statement;
    private Double total;

    public CallCharges getCallCharges() {
        return this.callCharges;
    }

    public void setCallCharges(CallCharges callCharges) {
        this.callCharges = callCharges;
    }

    //package is a reserved word in Java so renamed to skyPackage to prevent issues in jsp.
    @JsonProperty("package")
    public Package getSkyPackage() {
        return this.skyPackage;
    }

    @JsonProperty("package")
    public void setSkyPackage(Package skyPackage) {
        this.skyPackage = skyPackage;
    }

    public SkyStore getSkyStore() {
        return this.skyStore;
    }

    public void setSkyStore(SkyStore skyStore) {
        this.skyStore = skyStore;
    }

    public Statement getStatement() {
        return this.statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public Double getTotal() {
        return this.total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bill bill = (Bill) o;

        if (callCharges != null ? !callCharges.equals(bill.callCharges) : bill.callCharges != null) return false;
        if (skyPackage != null ? !skyPackage.equals(bill.skyPackage) : bill.skyPackage != null) return false;
        if (skyStore != null ? !skyStore.equals(bill.skyStore) : bill.skyStore != null) return false;
        if (statement != null ? !statement.equals(bill.statement) : bill.statement != null) return false;
        return !(total != null ? !total.equals(bill.total) : bill.total != null);

    }

    @Override
    public int hashCode() {
        int result = callCharges != null ? callCharges.hashCode() : 0;
        result = 31 * result + (skyPackage != null ? skyPackage.hashCode() : 0);
        result = 31 * result + (skyStore != null ? skyStore.hashCode() : 0);
        result = 31 * result + (statement != null ? statement.hashCode() : 0);
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }


}
