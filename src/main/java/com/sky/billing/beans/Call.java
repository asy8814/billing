package com.sky.billing.beans;

public class Call {
    private String called;
    private Double cost;
    private String duration;

    public String getCalled() {
        return this.called;
    }

    public void setCalled(String called) {
        this.called = called;
    }

    public Double getCost() {
        return this.cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Call call = (Call) o;

        if (called != null ? !called.equals(call.called) : call.called != null) return false;
        if (cost != null ? !cost.equals(call.cost) : call.cost != null) return false;
        return !(duration != null ? !duration.equals(call.duration) : call.duration != null);

    }

    @Override
    public int hashCode() {
        int result = called != null ? called.hashCode() : 0;
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        return result;
    }
}
