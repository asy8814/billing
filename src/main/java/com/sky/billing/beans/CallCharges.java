package com.sky.billing.beans;

import java.util.List;

public class CallCharges {
    private List<Call> calls;
    private Double total;

    public List<Call> getCalls() {
        return this.calls;
    }

    public void setCalls(List<Call> calls) {
        this.calls = calls;
    }

    public Double getTotal() {
        return this.total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CallCharges that = (CallCharges) o;

        if (calls != null ? !calls.equals(that.calls) : that.calls != null) return false;
        return !(total != null ? !total.equals(that.total) : that.total != null);

    }

    @Override
    public int hashCode() {
        int result = calls != null ? calls.hashCode() : 0;
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }
}
