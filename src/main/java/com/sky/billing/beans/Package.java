package com.sky.billing.beans;

import java.util.List;

public class Package {
    private List<Subscription> subscriptions;
    private Double total;

    public List<Subscription> getSubscriptions() {
        return this.subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public Double getTotal() {
        return this.total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Package aPackage = (Package) o;

        if (subscriptions != null ? !subscriptions.equals(aPackage.subscriptions) : aPackage.subscriptions != null)
            return false;
        return !(total != null ? !total.equals(aPackage.total) : aPackage.total != null);

    }

    @Override
    public int hashCode() {
        int result = subscriptions != null ? subscriptions.hashCode() : 0;
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }
}
