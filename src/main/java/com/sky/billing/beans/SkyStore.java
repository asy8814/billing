package com.sky.billing.beans;

import java.util.List;

public class SkyStore {
    private List<BuyAndKeep> buyAndKeep;
    private List<Rental> rentals;
    private Double total;

    public List<BuyAndKeep> getBuyAndKeep() {
        return this.buyAndKeep;
    }

    public void setBuyAndKeep(List<BuyAndKeep> buyAndKeep) {
        this.buyAndKeep = buyAndKeep;
    }

    public List<Rental> getRentals() {
        return this.rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    public Double getTotal() {
        return this.total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SkyStore skyStore = (SkyStore) o;

        if (buyAndKeep != null ? !buyAndKeep.equals(skyStore.buyAndKeep) : skyStore.buyAndKeep != null) return false;
        if (rentals != null ? !rentals.equals(skyStore.rentals) : skyStore.rentals != null) return false;
        return !(total != null ? !total.equals(skyStore.total) : skyStore.total != null);

    }

    @Override
    public int hashCode() {
        int result = buyAndKeep != null ? buyAndKeep.hashCode() : 0;
        result = 31 * result + (rentals != null ? rentals.hashCode() : 0);
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }
}
