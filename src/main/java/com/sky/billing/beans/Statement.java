package com.sky.billing.beans;

import java.util.Date;

public class Statement {
    private Date due;
    private Date generated;
    private Period period;

    public Date getDue() {
        return this.due;
    }

    public void setDue(Date due) {
        this.due = due;
    }

    public Date getGenerated() {
        return this.generated;
    }

    public void setGenerated(Date generated) {
        this.generated = generated;
    }

    public Period getPeriod() {
        return this.period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Statement statement = (Statement) o;

        if (due != null ? !due.equals(statement.due) : statement.due != null) return false;
        if (generated != null ? !generated.equals(statement.generated) : statement.generated != null) return false;
        return !(period != null ? !period.equals(statement.period) : statement.period != null);

    }

    @Override
    public int hashCode() {
        int result = due != null ? due.hashCode() : 0;
        result = 31 * result + (generated != null ? generated.hashCode() : 0);
        result = 31 * result + (period != null ? period.hashCode() : 0);
        return result;
    }
}
