package com.sky.billing.controller;

import com.sky.billing.beans.Bill;
import com.sky.billing.service.BillingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BillController {

    private static final Logger LOG = Logger.getLogger(BillController.class);
    @Autowired
    private BillingService service;

    @RequestMapping(name = "/bill.html", method = RequestMethod.GET)
    public ModelAndView handleGet() {
        ModelAndView mav = new ModelAndView("bill");
        Bill bill = service.fetchBillingInformation();
        if (bill == null) {
            LOG.warn("Unable to retrieve customer's bill");
            //something went wrong so send customer to error page
            mav.setViewName("error");
        } else {
            mav.addObject("bill", bill);
        }
        return mav;
    }


    public BillingService getService() {
        return service;
    }

    public void setService(BillingService service) {
        this.service = service;
    }
}
