package com.sky.billing.service;

import com.sky.billing.beans.Bill;


public interface BillingService {

    /**
     * Retrieves the Billing Information - in the real world, we would need to pass some customer info in here.
     */
    Bill fetchBillingInformation();
}
