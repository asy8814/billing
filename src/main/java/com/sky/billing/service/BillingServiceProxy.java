package com.sky.billing.service;

import com.sky.billing.beans.Bill;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.URL;

/**
 * A simple service to be utilised as a proxy to the billing JSON endpoint
 */
public class BillingServiceProxy implements BillingService {
    private static final Logger LOG = Logger.getLogger(BillingService.class);
    private URL endpoint;

    private ObjectMapper mapper;

    /**
     * Obtains the billing information. In the real world, we would need to pass
     * some customer and billing period information in to this method.
     *
     * @return A Bill containing the customer's billing information for this period. Or null
     * if there was a problem retrieving the data
     */
    public Bill fetchBillingInformation() {
        if (endpoint != null) {
            //use jackson to convert the json from the endpoint in to Java objects
            Bill bill = null;
            try {
                bill = mapper.readValue(endpoint, Bill.class);
            } catch (IOException e) {
                LOG.error(String.format("Unable to retrieve bill details from %s", endpoint), e);
            }
            return bill;
        }
        return null;
    }

    /**
     * Sets the endpoint for the JSON location
     *
     * @param endpoint A URL string for the JSON location
     */
    public void setEndpoint(URL endpoint) {
        this.endpoint = endpoint;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }
}
