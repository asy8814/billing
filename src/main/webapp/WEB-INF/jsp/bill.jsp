<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="<c:url value='css/custom.css'/>" rel="stylesheet">
        <script src="<c:url value='js/jquery-1.11.3.min.js'/>"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="<c:url value='js/bill.js'/>"></script>
        <title>Your Sky Bill</title>
    </head>
    <body>
        <jsp:include page="header.jsp" />
        <div class="container">
        <div>
            <h2>Your Bill</h2>
        </div>
        <jsp:include page="billing/alert.jsp"/>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <jsp:include page="billing/skyPackage.jsp"/>
            </div>
            <div class="col-xs-12 col-sm-6">
                <jsp:include page="billing/skyStore.jsp"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <jsp:include page="billing/phoneCalls.jsp"/>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="title">Grand Total</span>
                    </div>
                    <div class="panel-body">
                        <span class="total pull-right">&pound;${bill.total}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Questions About Your Bill?</h3>
                        <p>Call us now on 0330 000000</p>
                        <p>Or alternatively, use our <a href="#">Live Chat</a> facility.</p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

