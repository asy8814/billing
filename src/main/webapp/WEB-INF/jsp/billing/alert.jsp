<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="alert alert-info">
   <span>
      This bill was generated on
      <fmt:formatDate value="${bill.statement.generated}" pattern="dd MMM yyyy"/>
      .
      Your payment of <strong>&pound;${bill.total}</strong> is due by
      <strong>
         <fmt:formatDate value="${bill.statement.due}" pattern="dd MMM yyyy"/>
      </strong>
   </span>
</div>

