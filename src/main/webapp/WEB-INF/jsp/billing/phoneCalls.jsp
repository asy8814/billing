<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="panel panel-default ">
    <div class="panel-heading">
        <a role="button" data-toggle="collapse" href="#phoneCalls" class="collapsed">
            <div class="row">
                <div class="col-md-4"><span class="title">Phone Calls</span></div>
                <div class="col-md-8"><i class="icon-2x icon-expand-alt pull-right"></i></div>
            </div>
        </a>
    </div>
    <div class="panel-body collapse" id="phoneCalls">
        <c:forEach items="${bill.callCharges.calls}" var="c">
            <div class="row">
                <div class="col-md-4"><strong>${c.called}</strong></div>
                <div class="col-md-2">${c.duration}</div>
                <div class="col-md-2 pull-right">
                    &pound;
                    <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${c.cost}" />
                </div>
            </div>
            <hr/>
        </c:forEach>
    </div>
    <div class="panel-body">
        <span class="total pull-right">
            Sub Total: &pound;
            <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${bill.callCharges.total}" />
        </span>
    </div>
</div>

