<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="panel panel-default ">
    <div class="panel-heading">
        <a role="button" data-toggle="collapse" href="#skyPackage" class="collapsed">
            <div class="row">
                <div class="col-sm-4">
                    <span class="title">Your package</span>
                </div>
                <div class="col-sm-4">
                    <span>
                        (
                        <fmt:formatDate value="${bill.statement.period.from}" pattern="dd MMM"/>
                        -
                        <fmt:formatDate value="${bill.statement.period.to}" pattern="dd MMM"/>
                        )
                    </span>
                </div>
                <div class="col-sm-4">
                    <i class="icon-2x icon-expand-alt pull-right"></i>
                </div>
            </div>
        </a>
    </div>
    <div class="panel-body collapse" id="skyPackage">
        <c:forEach items="${bill.skyPackage.subscriptions}" var="sub">
            <div class="row">
                <div class="col-sm-4"><strong>${fn:toUpperCase(sub.type)}</strong></div>
                <div class="col-sm-4">${sub.name}</div>
                <div class="col-sm-2 pull-right">
                    &pound;
                    <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${sub.cost}" />
                </div>
            </div>
            <hr/>
        </c:forEach>
    </div>
    <div class="panel-body">
        <span class="total pull-right">
            Sub Total: &pound;
            <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${bill.skyPackage.total}" />
        </span>
    </div>
</div>

