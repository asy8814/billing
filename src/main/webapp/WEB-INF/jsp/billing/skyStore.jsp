<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="panel panel-default ">
    <div class="panel-heading">
        <a role="button" data-toggle="collapse" href="#skyStore" class="collapsed">
            <div class="row">
                <div class="col-sm-4"><span class="title">Sky Store</span></div>
                <div class="col-sm-8"><i class="icon-2x icon-expand-alt pull-right"></i></div>
            </div>
        </a>
    </div>
    <div class="panel-body collapse" id="skyStore">
        <c:forEach items="${bill.skyStore.buyAndKeep}" var="b">
            <div class="row">
                <div class="col-sm-2"><span>Buy And Keep</span></div>
                <div class="col-sm-8"><span><strong>${fn:toUpperCase(b.title)}</strong></span></div>
                <div class="col-sm-2">
                    <span>
                        &pound;
                        <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${b.cost}" />
                    </span>
                </div>
            </div>
            <hr/>
        </c:forEach>
        <c:forEach items="${bill.skyStore.rentals}" var="r">
            <div class="row">
                <div class="col-sm-2"><span>Rental</span></div>
                <div class="col-sm-8"><span><strong>${fn:toUpperCase(r.title)}</strong></span></div>
                <div class="col-sm-2 pull-right">
                    <span>
                        &pound;
                        <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${r.cost}" />
                    </span>
                </div>
            </div>
            <hr/>
        </c:forEach>
    </div>
    <div class="panel-body">
        <span class="total pull-right">
            Sub Total: &pound;
            <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${bill.skyStore.total}" />
        </span>
    </div>
</div>

