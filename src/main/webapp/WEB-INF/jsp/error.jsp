<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <title>Your Sky Bill</title>
    </head>
    <body>
        <jsp:include page="header.jsp" />
        <div class="container">
            <h2>Something Went Wrong</h2>
            <h4>We are currently unable to display your billing information. Please try again later.
        </div>

    </body>
</html>