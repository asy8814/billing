<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header>
  <div class="container">
     <img src="<c:url value='img/logo.png'/>" width="120px"/>
  </div>
</header>
<nav class="navbar navbar-md" role="navigation">
  <div class="container">
     <ul class="nav navbar-nav">
        <li><a href="#">TV Guide</a></li>
        <li><a href="#">My Account</a></li>
        <li><a href="#">Upgrades</a></li>
        <li><a href="#">Help & Support</a></li>
     </ul>
  </div>
</nav>