$(document).ready(function(){
    //swap the icon when the contents are shown/hidden
    $('.collapsed').on('click', function(){
        var icon = $(this).find('i').first();
        icon.toggleClass('icon-expand-alt');
        icon.toggleClass('icon-collapse-alt');
    });
});