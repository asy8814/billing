package com.sky.billing.controller;

import com.sky.billing.beans.Bill;
import com.sky.billing.controller.BillController;
import com.sky.billing.service.BillingServiceProxy;
import com.sky.billing.service.helpers.BillBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


public class BillControllerTest {


    @Mock
    private BillingServiceProxy service;

    private BillController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(service.fetchBillingInformation()).thenReturn(BillBuilder.buildSimpleBill());
        controller = new BillController();
        controller.setService(service);

    }

    @Test
    public void viewNameShouldBeBill() {
        ModelAndView mav = controller.handleGet();
        assertThat(mav.getViewName(), equalTo("bill"));
    }

    @Test
    public void whenNullIsReturnedFromServiceNoBillIsReturnedAndPageIsError() throws IOException {
        when(service.fetchBillingInformation()).thenReturn(null);

        ModelAndView mav = controller.handleGet();
        Object o = mav.getModelMap().get("bill");
        assertNull(o);
        assertThat(mav.getViewName(), equalTo("error"));
    }

    @Test
    public void billObjectInModelAndViewShouldMatchServiceResponse() throws IOException {

        //mock out the service so its not depended on any endpoint - we always give a complex bill to the controller.
        when(service.fetchBillingInformation()).thenReturn(BillBuilder.buildComplexBill());

        ModelAndView mav = controller.handleGet();
        Object o = mav.getModelMap().get("bill");
        assertNotNull(o);
        assertTrue(o instanceof Bill);
        Bill bill = (Bill) o;
        assertThat(bill, equalTo(BillBuilder.buildComplexBill()));
    }


}
