package com.sky.billing.controller;


import com.sky.billing.controller.ErrorController;
import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ErrorControllerTest {

    @Test
    public void errorControllerShouldGiveErrorView() {
        ErrorController controller = new ErrorController();
        assertThat(controller.handleGet().getViewName(), equalTo("error"));
    }

}
