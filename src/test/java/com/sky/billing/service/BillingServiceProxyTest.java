package com.sky.billing.service;

import com.sky.billing.beans.Bill;
import com.sky.billing.service.helpers.BillBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;


public class BillingServiceProxyTest {

    private BillingServiceProxy service;

    @Before
    public void setup() {
        service = new BillingServiceProxy();
        service.setMapper(new ObjectMapper());
    }

    @Test
    public void nullEndPointShouldReturnNullObject() {
        assertNull(service.fetchBillingInformation());
    }

    @Test
    public void anEndPointShouldReturnSomeData() throws MalformedURLException, URISyntaxException {
        //use a test resource url so don't rely on the actual endpoint during unit testing
        service.setEndpoint(getURLFromResource("/sample.json"));
        checkBasicFunctionality();
    }

    /**
     * Validates with assertions that the service can call it's preferred endpoint, returning some data
     */
    private Bill checkBasicFunctionality() {
        Bill bill = service.fetchBillingInformation();
        assertNotNull(bill);
        return bill;
    }

    /**
     * Gets the URL and worksaround the known quirk of spaces in urls - just in case the the project is
     * on a path with spaces in.
     */
    private URL getURLFromResource(String resource) throws URISyntaxException, MalformedURLException {
        return new URL(this.getClass().getResource(resource).toString().replaceAll("%20", " "));
    }

    @Test
    public void aSimpleBillShouldReturnTheCorrectData() throws URISyntaxException, MalformedURLException {
        //The json in this test is a cut down version with no calls, or individual purchases - just packages
        service.setEndpoint(getURLFromResource("/small-sample.json"));
        Bill bill = checkBasicFunctionality();
        Bill expectedBill = BillBuilder.buildSimpleBill();
        assertThat(bill, equalTo(expectedBill));
    }

    @Test
    public void aComplexBillShouldReturnTheCorrectData() throws MalformedURLException, URISyntaxException {
        //The json in this test is a full bill which includes all fields pupulated (includes optional extras)
        service.setEndpoint(getURLFromResource("/big-sample.json"));
        Bill bill = checkBasicFunctionality();
        Bill expectedBill = BillBuilder.buildComplexBill();
        assertThat(bill, equalTo(expectedBill));
    }

    @Test
    public void whenTheServiceReturnsInvalidJsonANullBillShouldBeReturned() throws MalformedURLException, URISyntaxException {
        service.setEndpoint(getURLFromResource("/sample-broken.json"));
        Bill bill = service.fetchBillingInformation();
        assertNull(bill);
    }

    @Test
    public void whenTheServiceIsWrongEndpointTheBillShouldBeNull() {
        service.setEndpoint(this.getClass().getResource("/notfound.json"));
        Bill bill = service.fetchBillingInformation();
        assertNull(bill);
    }


}
