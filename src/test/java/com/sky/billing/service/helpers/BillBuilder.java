package com.sky.billing.service.helpers;

import com.sky.billing.beans.*;
import com.sky.billing.beans.Package;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * A convenience test class for building various billing components (reduces the need for lots of duplicate calls to setter
 * during tests). Error handling around nulls etc has not been considered here as it is for test purposes.
 */
public class BillBuilder {

    //SimpleDateFormat is not thread safe. as this is a test class only running from a single thread, then it is OK,
    //but do not use this in a production environment
    private static final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    private static Logger LOG = Logger.getLogger(BillBuilder.class);

    static {
        //jackson uses GMT by default so lets keep things consistent
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    /**
     * Builds the statement and its child period object. All dates are in strings in the format yyyy-mm-dd
     *
     * @param due       The date the payment is due
     * @param generated The date that the bill was produced
     * @param from      The start date of this bill period
     * @param to        The end date of this bill period
     * @return the statement produced from the provided data
     */
    public static Statement buildStatement(Date due, Date generated, Date from, Date to) {
        Statement statement = new Statement();
        statement.setDue(due);
        statement.setGenerated(generated);


        Period period = new Period();
        period.setFrom(from);
        period.setTo(to);
        statement.setPeriod(period);
        return statement;
    }

    public static Subscription buildSubscription(String type, String name, Double cost) {
        Subscription s = new Subscription();
        s.setCost(cost);
        s.setName(name);
        s.setType(type);
        return s;
    }

    /**
     * Produces a bill which doesnt have any of the optional extras (just basic packages). It's
     * a java representation of small-sample.json
     */
    public static Bill buildSimpleBill() {
        Bill bill = new Bill();
        Statement statement = null;
        try {
            statement = BillBuilder.buildStatement(
                    (dateFormatter).parse("2015-06-29"),
                    (dateFormatter).parse("2015-06-27"),
                    (dateFormatter).parse("2015-05-26"),
                    (dateFormatter).parse("2015-06-25"));
        } catch (ParseException e) {
            LOG.error("There was a problem parsing the dates", e);
        }

        bill.setStatement(statement);
        bill.setTotal(123.04);

        List<Subscription> subs = new ArrayList<Subscription>();
        subs.add(BillBuilder.buildSubscription("tv", "The Full Package", 120.00));
        subs.add(BillBuilder.buildSubscription("talk", "Sky Talk Anytime Discounted", 3.04));

        com.sky.billing.beans.Package p = new Package();
        p.setTotal(123.04);
        p.setSubscriptions(subs);

        bill.setSkyPackage(p);
        return bill;
    }

    /**
     * Build a bill which includes packages as well as phone calls and paid extras. based on big-sample.json
     */
    public static Bill buildComplexBill() {
        //start with the basic bill
        Bill bill = buildSimpleBill();

        //overwrite the total previous set
        bill.setTotal(152.27);

        CallCharges callCharges = new CallCharges();
        List<Call> calls = new ArrayList<Call>();
        calls.add(buildCall("07716393769", "00:23:03", 2.13));
        calls.add(buildCall("02074351359", "00:23:03", 2.13));
        callCharges.setCalls(calls);

        callCharges.setTotal(4.26);

        bill.setCallCharges(callCharges);

        SkyStore store = new SkyStore();
        store.setTotal(24.97);
        List<BuyAndKeep> buyAndKeep = new ArrayList<BuyAndKeep>();
        buyAndKeep.add(buildBuyAndKeep("That's what she said", 9.99));
        buyAndKeep.add(buildBuyAndKeep("Broke back mountain", 9.99));
        store.setBuyAndKeep(buyAndKeep);

        List<Rental> rentals = new ArrayList<Rental>();
        rentals.add(buildRental("50 Shades of Grey", 4.99));

        store.setRentals(rentals);

        bill.setSkyStore(store);

        return bill;

    }

    /**
     * Builds a Rental purchase and returns it
     *
     * @param title The name of the rental
     * @param cost  The cost of the rental
     * @return A Rental object containing the provided information
     */
    private static Rental buildRental(String title, double cost) {
        Rental r = new Rental();
        r.setTitle(title);
        r.setCost(cost);
        return r;
    }

    /**
     * Creates a Buy And Keep purchase and returns it
     *
     * @param title The title of the Buy And Keep item
     * @param cost  The cost of the item
     * @return A BuyAndKeep object containing the provided information
     */
    private static BuyAndKeep buildBuyAndKeep(String title, double cost) {
        BuyAndKeep bak = new BuyAndKeep();
        bak.setCost(cost);
        bak.setTitle(title);
        return bak;
    }

    /**
     * Creates a phone call record with the provided data and returns is
     *
     * @param number   The phone number called
     * @param duration The length of the call
     * @param cost     The cost of the call
     * @return The create Call object from the provided data
     */
    private static Call buildCall(String number, String duration, double cost) {
        Call c = new Call();
        c.setCost(cost);
        c.setCalled(number);
        c.setDuration(duration);
        return c;
    }
}
